-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2022 at 03:38 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rns_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `USER_ID` int(11) NOT NULL,
  `FNAME` varchar(100) NOT NULL,
  `LNAME` varchar(100) NOT NULL,
  `GENDER` varchar(20) NOT NULL,
  `DOB` varchar(20) NOT NULL,
  `PHONE` varchar(20) NOT NULL,
  `pass` varchar(250) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `ADDRESS` varchar(440) NOT NULL,
  `AADHAAR` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`USER_ID`, `FNAME`, `LNAME`, `GENDER`, `DOB`, `PHONE`, `pass`, `EMAIL`, `ADDRESS`, `AADHAAR`) VALUES
(1001, 'Vijay', 'Penu', 'Male', '2001-06-21', '9754745214', 'tiger', 'vijay@gmail.com', 'Bangalore.', '9898776763'),
(1002, 'Ramesh', 'Karuti', 'Male', '1995-01-20', '9741720721', 'tiger', 'ramesh.k@gmail.com', 'Bangalore.79', '87647632324'),
(1006, 'Vivek', 'Ch', 'Male', '1980-02-03', '+919741720720', 'tiger', 'ramesh.karuti@yahoo.co.in', 'Bangalore.', '9787676565'),
(1007, 'Ullas', 'K', 'Male', '1988-02-03', '+919741720720', 'tiger', 'ramesh.karuti@yahoo.co.in', 'Bangalore.', '9898787676'),
(1008, 'Suresh', 'CK', 'Male', '1986-10-01', '+919741720720', 'tiger', 'ramesh.karuti@yahoo.co.in', 'Bangalore', '98787676476'),
(1009, 'Ullas', 'K', 'Male', '2022-03-21', '9741720720', 'tiger', 'ramesh.karuti@gmail.com', 'Bangalore', '87647632323'),
(1010, 'Sharan', 'H', 'Male', '1980-03-03', '+919741720222', 'tiger', 'sharan@yahoo.co.in', 'Bangalore.', '9787676444');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`USER_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `USER_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1011;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
