<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WebApp - Register</title>
</head>
<body>

	<%
		ArrayList users = new ArrayList();
		users = (ArrayList) request.getAttribute("users");
	%>
	<h2>Edit User</h2>
	<form action="UpdateUserServlet" method="post">
		<table>
			<tr>
				<td>User ID</td>
				<td><input type="text" name="uid" value="<%=users.get(0)%>"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td>Enter First Name</td>
				<td><input type="text" name="fname" value="<%=users.get(1)%>"></td>
			</tr>
			<tr>
				<td>Enter Last Name</td>
				<td><input type="text" name="lname" value="<%=users.get(2)%>"></td>
			</tr>
			<tr>
				<td>Select Gender</td>
				<td><select name="gender">
						<%
							if (users.get(3).equals("Male")) {
						%>
						<option value="Male" selected="selected">Male</option>
						<option value="Female">Female</option>
						<%
							} else {
						%>
						<option value="Male">Male</option>
						<option value="Female" selected="selected">Female</option>
						<%
							}
						%>
				</select></td>
			</tr>
			<tr>
				<td>Enter DOB</td>
				<td><input type="date" name="dob" value='<%=users.get(4)%>'></td>
			</tr>
			<tr>
				<td>Enter Phone</td>
				<td><input type="text" name="phone" value="<%=users.get(5)%>"></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><input type="password" name="pass"
					value="<%=users.get(6)%>"></td>
			</tr>
			<tr>
				<td>Enter Email</td>
				<td><input type="email" name="email" value="<%=users.get(7)%>"></td>
			</tr>
			<tr>
				<td>Enter Aadhaar</td>
				<td><input type="text" name="aadhaar" value="<%=users.get(9)%>"></td>
			</tr>
			<tr>
				<td>Enter Address</td>
				<td><textarea name="address" value="" cols="21" rows="3"
						draggable="false"><%=users.get(8)%></textarea></td>
			</tr>
			<tr>
				<td><input type="reset" value="Clear"></td>
				<td><input type="submit" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>