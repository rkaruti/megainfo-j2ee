<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
		ArrayList users = new ArrayList();
		users = (ArrayList) request.getAttribute("users");
		//out.println("Users " + users);
	%>

	<table>
		<tr>
			<th>UID</th>
			<td>First Name</td>
			<td>Last Name</td>
			<td>Gender</td>
			<td>DOB</td>
			<td>Phone</td>
			<td>Password</td>
			<td>Email</td>
			<td>Address</td>
			<td>Aadhaar</td>
			<td>Edit</td>
		</tr>
		<%
			for (int i = 0; i < users.size(); i = i + 10) {
		%>
		<tr>
			<td><%=users.get(i)%></td>
			<td><%=users.get(i + 1)%></td>
			<td><%=users.get(i + 2)%></td>
			<td><%=users.get(i + 3)%></td>
			<td><%=users.get(i + 4)%></td>
			<td><%=users.get(i + 5)%></td>
			<td><%=users.get(i + 6)%></td>
			<td><%=users.get(i + 7)%></td>
			<td><%=users.get(i + 8)%></td>
			<td><%=users.get(i + 9)%></td>
			<td><a href="EditUserServlet?uid=<%=users.get(i)%>">Edit</a> </td>
		</tr>
		<%
			}
		%>
	</table>
</body>
</html>