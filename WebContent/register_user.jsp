<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WebApp - Register</title>
</head>
<body>
	<h2>Register User</h2>
	<form action="RegisterUserServlet" method="post">
		<table>
			<tr>
				<td>Enter First Name</td>
				<td><input type="text" name="fname"></td>
			</tr>
			<tr>
				<td>Enter Last Name</td>
				<td><input type="text" name="lname"></td>
			</tr>
			<tr>
				<td>Select Gender</td>
				<td><select name="gender">
						<option value="Male">Male</option>
						<option value="Female">Female</option>
				</select></td>
			</tr>
			<tr>
				<td>Enter DOB</td>
				<td><input type="date" name="dob"></td>
			</tr>
			<tr>
				<td>Enter Phone</td>
				<td><input type="text" name="phone"></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><input type="password" name="pass"></td>
			</tr>
			<tr>
				<td>Enter Email</td>
				<td><input type="email" name="email"></td>
			</tr>
			<tr>
				<td>Enter Aadhaar</td>
				<td><input type="text" name="aadhaar"></td>
			</tr>
			<tr>
				<td>Enter Address</td>
				<td><textarea name="address" cols="21" rows="3"
						draggable="false"></textarea></td>
			</tr>
			<tr>
				<td><input type="reset" value="Clear"></td>
				<td><input type="submit" value="Register"></td>
			</tr>
		</table>
	</form>
</body>
</html>