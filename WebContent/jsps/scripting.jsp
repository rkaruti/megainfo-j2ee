<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Demo Scripting Element</title>
</head>
<body>
	<h2>Scriptlet, Declarative and Expression</h2>
	<%!int x = 10, y = 20, sum = 0;%>
	<%
		sum = x + y;
	%>
	Sum is : <%=sum%>
</body>
</html>