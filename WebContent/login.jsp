<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>WebApp - Login</title>
</head>
<body>
<body>
	<form action="LoginServlet" method="post">
		<table>
			<tr>
				<td>Enter Name</td>
				<td><input type="text" name="uname"></td>
			</tr>
			<tr>
				<td>Enter Password</td>
				<td><input type="password" name="upass"></td>
			</tr>

			<tr>
				<td><input type="hidden" name="utype" value="admin" /></td>
			</tr>
			<tr>
				<td><input type="reset" value="Clear"></td>
				<td><input type="submit" value="Login"></td>
			</tr>
		</table>
	</form>
</body>
</body>

</html>