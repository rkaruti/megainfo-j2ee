package com.megainfo.demo.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.megainfo.demo.dao.RegisterUserDao;
import com.megainfo.demo.model.User;

/**
 * Servlet implementation class UpdateUserServlet
 */
@WebServlet("/UpdateUserServlet")
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	RegisterUserDao regUserDao;

	public void init() {
		regUserDao = new RegisterUserDao();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String uid = request.getParameter("uid");
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String gender = request.getParameter("gender");
		String dob = request.getParameter("dob");
		String phone = request.getParameter("phone");
		String pass = request.getParameter("pass");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String aadhaar = request.getParameter("aadhaar");

		User regUser = new User();
		regUser.setId(uid);
		regUser.setFname(fname);
		regUser.setLname(lname);
		regUser.setGender(gender);
		regUser.setDob(dob);
		regUser.setPhone(phone);
		regUser.setPass(pass);
		regUser.setEmail(email);
		regUser.setAddress(address);
		regUser.setAadhaar(aadhaar);

		int regState = regUserDao.updateUser(regUser);
		RequestDispatcher rd;
		if (regState == 1) {
			rd = request.getRequestDispatcher("ListUserServlet");
			rd.forward(request, response);
		} else {
			response.sendRedirect("error.jsp");
		}
	}

}
