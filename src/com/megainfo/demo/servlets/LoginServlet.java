package com.megainfo.demo.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.megainfo.demo.dao.LoginUserDao;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("uname");
		String pass = request.getParameter("upass");
		
		HttpSession session = request.getSession(true);

		LoginUserDao loginUser = new LoginUserDao();
		ArrayList list = loginUser.getUser(name, pass);

		if (!list.isEmpty()) {
			session.setAttribute("name", list.get(0));

			RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
			rd.forward(request, response);
		} else {
			response.sendRedirect("error.jsp");
		}
	}
}
