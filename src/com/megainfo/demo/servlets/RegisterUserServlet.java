package com.megainfo.demo.servlets;

import java.io.IOException;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.megainfo.demo.dao.RegisterUserDao;
import com.megainfo.demo.model.User;

/**
 * Servlet implementation class RegisterUserServlet
 */
@WebServlet("/RegisterUserServlet")
public class RegisterUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	RegisterUserDao regUserDao;
	
	public void init() {
		regUserDao = new RegisterUserDao();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String gender = request.getParameter("gender");
		String dob = request.getParameter("dob");
		String phone = request.getParameter("phone");
		String pass = request.getParameter("pass");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String aadhaar = request.getParameter("aadhaar");
		
		User regUser = new User();
		regUser.setFname(fname);
		regUser.setLname(lname);
		regUser.setGender(gender);
		regUser.setDob(dob);
		regUser.setPhone(phone);
		regUser.setPass(pass);
		regUser.setEmail(email);
		regUser.setAddress(address);
		regUser.setAadhaar(aadhaar);
		
		int regState = regUserDao.regiserUser(regUser);
		RequestDispatcher rd;
		if (regState == 1) {
			rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		} else {
			response.sendRedirect("error.jsp");
		}
		
	}

}
