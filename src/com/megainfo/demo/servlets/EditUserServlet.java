package com.megainfo.demo.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.megainfo.demo.dao.EditUser;

/**
 * Servlet implementation class EditUserServlet
 */
@WebServlet("/EditUserServlet")
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String uid = request.getParameter("uid");
		EditUser obj = new EditUser();
		ArrayList list = obj.getUser(uid);
		RequestDispatcher rd;

		if(!list.isEmpty()){
			request.setAttribute("users", list);
			rd = request.getRequestDispatcher("edit_user.jsp");
			rd.forward(request, response);
		}
		
	}
}
