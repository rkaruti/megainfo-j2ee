/**
 * 
 */
package com.megainfo.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.megainfo.demo.db.DBConnection;
import com.megainfo.demo.model.User;

/**
 * @author rkaruti
 *
 */
public class RegisterUserDao {

	public int regiserUser(User user) {
		int result = 0;
		String regQuery = "INSERT INTO USERS (FNAME, LNAME, GENDER, DOB, PHONE, PASS, EMAIL, ADDRESS, AADHAAR) VALUES (?,?,?,?,?,?,?,?,?)";
		try {
			// Connect to Database
			Connection con = DBConnection.getDBConnection();
			// Create the Statement
			PreparedStatement ps = con.prepareStatement(regQuery);
			ps.setString(1, user.getFname());
			ps.setString(2, user.getLname());
			ps.setString(3, user.getGender());
			ps.setString(4, user.getDob());
			ps.setString(5, user.getPhone());
			ps.setString(6, user.getPass());
			ps.setString(7, user.getEmail());
			ps.setString(8, user.getAddress());
			ps.setString(9, user.getAadhaar());
			// Execute Query
			result = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public int updateUser(User user) {
		int result = 0;
		String query = "UPDATE USERS SET FNAME = ?, LNAME = ?, GENDER = ?, DOB = ?, PHONE = ?, PASS = ?, EMAIL = ?, ADDRESS = ?, AADHAAR = ? WHERE USER_ID = ?";
		try {
			// Connect to Database
			Connection con = DBConnection.getDBConnection();
			// Create the Statement
			PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, user.getFname());
			ps.setString(2, user.getLname());
			ps.setString(3, user.getGender());
			ps.setString(4, user.getDob());
			ps.setString(5, user.getPhone());
			ps.setString(6, user.getPass());
			ps.setString(7, user.getEmail());
			ps.setString(8, user.getAddress());
			ps.setString(9, user.getAadhaar());
			ps.setString(10, user.getId());
			// Execute Query
			result = ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
