package com.megainfo.demo.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.megainfo.demo.db.DBConnection;

public class EditUser {
	Connection con;
	ArrayList list;

	public ArrayList<String> getUser(String id) {

		try {
			con = DBConnection.getDBConnection();
			Statement ps = con.createStatement();
			ResultSet rs = ps.executeQuery("SELECT * FROM USERS WHERE USER_ID = " + id + "");
			list = new ArrayList<String>();

			while (rs.next()) {
				list.add(rs.getString(1));
				list.add(rs.getString(2));
				list.add(rs.getString(3));
				list.add(rs.getString(4));
				list.add(rs.getString(5));
				list.add(rs.getString(6));
				list.add(rs.getString(7));
				list.add(rs.getString(8));
				list.add(rs.getString(9));
				list.add(rs.getString(10));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("USer " + list);
		return list;
	}

	public static void main(String arg[]) {
		EditUser obj = new EditUser();
		obj.getUser("1001");
	}
}
