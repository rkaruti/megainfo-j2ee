/**
 * 
 */
package com.megainfo.demo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.megainfo.demo.db.DBConnection;

/**
 * @author rkaruti
 *
 */
public class ListUserDao {
	Connection con;
	ArrayList<String> list;

	public ArrayList<String> listAllUsers() {

		try {
			con = DBConnection.getDBConnection();
			Statement ps = con.createStatement();
			ResultSet rs = ps.executeQuery("SELECT * FROM USERS");
			list = new ArrayList<String>();

			while (rs.next()) {
				list.add(rs.getString(1));
				list.add(rs.getString(2));
				list.add(rs.getString(3));
				list.add(rs.getString(4));
				list.add(rs.getString(5));
				list.add(rs.getString(6));
				list.add(rs.getString(7));
				list.add(rs.getString(8));
				list.add(rs.getString(9));
				list.add(rs.getString(10));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static void main(String arg[]) {
		ListUserDao obj = new ListUserDao();
		obj.listAllUsers();
	}
}
