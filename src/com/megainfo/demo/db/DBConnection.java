/**
 * 
 */
package com.megainfo.demo.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Ramesh Karuti
 *
 */
public class DBConnection {

	public static Connection getDBConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");// 1 Load the driver
			con = DriverManager.getConnection("jdbc:mysql://localhost/rns_demo", "root", ""); // 2 Make the connection
			System.out.println("Connection Success!!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return con;
	}

	public static void main(String[] args) {
		DBConnection.getDBConnection();

	}
}
