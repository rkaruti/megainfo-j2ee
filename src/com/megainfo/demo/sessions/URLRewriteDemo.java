package com.megainfo.demo.sessions;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class URLRewriteDemo
 */
@WebServlet("/URLRewriteDemo")
public class URLRewriteDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		String name = request.getParameter("uname");
		String pass = request.getParameter("upass");

		if (pass.equals("tiger")) {
			response.sendRedirect("URLRewriteCheck?user_name=" + name);
		}
	}

}
